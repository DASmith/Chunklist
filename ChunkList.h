//
// Created by Varick Erickson 2/3/2021.
//

#ifndef CHUNKLIST_CHUNKLIST_H
#define CHUNKLIST_CHUNKLIST_H

#include <iostream>

using namespace std;

const int ARRAY_SIZE = 8;

// These error classes are done. No modifications are required.
// See ChunkList_demo.cpp for an example of how they are used.
class InvalidArrayLength{
public:
    string Msg() {
        return "Invalid array length used for constructor.";
    }
};

class IndexOutOfBounds {
public:
    string Msg() {
        return "Index is out of bounds";
    }
};

class IteratorOutOfBounds {
public:
    string Msg() {
        return "Iterator is out of bounds.  Nothing left to iterate over.";
    }
};

class EmptyList {
public:
    string Msg() {
        return "List is empty";
    }
};

// Put your pre/post conditions here.

template<class T>
class ChunkList {
public:
    // Constructors
    ChunkList();
    //Function: Constructs an empty chunklist
    //Pre: Values have been declared in order to form nodes, iterators, and bookkeeping variables such as listLen and numChunks.
    //Post: An empty chunklist is created with all values being 0 and all pointers pointing to null.
    ChunkList(T arr[], int arrLen);
    //Function: Constructs a chunklist given an array of values and an array length
    //Pre: An array that is desired to go in the list exists and has a nonzero, nonnegative length
    //Post: The array is placed in the chunklist in separated nodes according to each node's array size

    // Deconstructor
    ~ChunkList();
    //Function: Deletes a whole chunklist
    //Pre: A chunklist has been constructed
    //Post: The chunklist has been completely deleted, node by node.

    // Add/remove elements
    void Append(T value);
    //Function: Adds a value to the end of the chunklist
    //Pre: A chunklist has been constructed
    //Post: The given value is put at the end of the chunklist, updating the length of the array and the length of the whole list
    void Remove(T value);
    //Function: Removes a single value from the chunklist
    //Pre: A non empty chunklist has been constructed
    //Post: The first instance of the given value is removed from the list and the total length of the list is updated

    // Stuff to find out about the ChunkList
    int GetLength();
    //Function: returns the total number of values in the whole list
    //Pre: A chunklist has been constructed
    //Post: Returns the total number of values which is the listLen private member.
    double LoadFactor();
    //Function: Calculates the total percentage of how much of the total nodes are storing values
    //Pre: A non empty chunklist has been constructed
    //Post: A percentage value is returned that gives the total percent of values being stored in all nodes. Ex: 1 node and 4 values => .5 is returned
    bool Contains(T value);
    //Function: This function tells the user if a given value exists in the list.
    //Pre: A non-empty chunklist has been constructed
    //Post: Function returns true if the given value is in the list and returns false if the value is not in the list.

    // Accessing elements
    T GetIndex(int i);
    //Function: Retrieves the value being stored at the given index
    //Pre: A non-empty chunklist has been constructed and the given index is non negative and not greater than or equal to listLen
    //Post: The value being housed in the given index is returned to the user, if the value is never found then IteratorOutOfBounds is thrown.
    void ResetIterator();
    //Function: Resets the private variables iterNode and arrPos
    //Pre: Variables for the iterator such as iterNode and arrPos have been declared.
    //Post: iterNode is set to the beginning of the list (head) and arrPos is set to 0 (first element of an array)
    T GetNextItem();
    //Function: Returns the value of next item in the list (starting with the very first one)
    //Pre: A non empty chunklist has been constructed with iterators set to the first node and first element of the array
    //Post: The value of the item is returned and the arrPos is moved up, if arrPos is incremented passed ARRAY_SIZE-1 then iterNode
    //is moved up to the next node and arrPos resets at 0.  An error message is thrown if there are no more items in the list.
    bool IsEmpty();
    //Function: Checks to see if a list has any nodes in it
    //Pre: A chunklist has been constructed
    //Post: Returns true if there are 0 nodes in a chunklist, returns false if nodes exist in the list
private:

    // Note we did not need to make this a template
    // since this struct is part of a template class.
    struct Node {
        Node* next;
        int len;
        T values[ARRAY_SIZE];
    };

    Node* head;
    Node* tail;

    // iterator
    Node* iterNode;     // What node are were currently on?
    int arrPos;         // Within the node, what element are we looking at?

    int listLen;        // Total elements
    int numChunks;      // Total chunks/nodes
};


#include "ChunkList.cpp"

#endif //CHUNKLIST_CHUNKLIST_H
