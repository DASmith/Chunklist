
#include "ChunkList.h"


template<class T>
ChunkList<T>::ChunkList() {     //Default constructor creates an empty list
    head = nullptr;
    tail = nullptr;
    iterNode = nullptr;
    arrPos = 0;
    listLen = 0;
    numChunks = 0;
}

template<class T>
ChunkList<T>::ChunkList(T *arr, int arrLen) {   //Constructor with 2 parameters allows us to create a list and insert an array of any positive valued length

    if(arrLen < 0){
        throw InvalidArrayLength();
    }

    head = nullptr;
    tail = nullptr;
    iterNode = head;
    arrPos = 0;
    listLen = 0;
    numChunks = 0;

    for(int i=0; i<arrLen; i++){
        Append(arr[i]);
        ResetIterator();
        }
}

template<class T>
ChunkList<T>::~ChunkList() {

}

template<class T>
void ChunkList<T>::Append(T value) {

   if(head == nullptr) {             //if the list is empty
        Node* newNode = new Node;    //create a new node
        newNode->next = nullptr;     //set the next pointer to null
        head = newNode;
        tail = newNode;              //set the tail to the new Node
        numChunks++;                 //update the number of nodes we have
        tail->values[0] = value;     //give the value to the first slot in the array
        tail->len = 1;               //update the length of the array
        listLen++;                   //update the total length
        return;
    } else {                                //if the list is not empty
       if((tail->len) < ARRAY_SIZE){        //if the length of the array of the last node has space
           tail->values[tail->len] = value; //put the value in that position
           tail->len++;                     //update the length of the array
           listLen++;                       //update the total length
           return;
       }
       if((tail->len) == ARRAY_SIZE) {      //if the length of the array of the last node is 8
           Node* newNode = new Node;        //create a new node
           newNode->next = nullptr;        //update the next pointer
           tail->next = newNode;           //move the tail along
           tail = tail->next;              //set the tail to newNode
           tail->values[0] = value;         //put the value in the first slot of the array of the new node
           tail->len = 1;                   //increase the length of the array
           numChunks++;                     //increase the number of nodes
           listLen++;                       //update the total length
           return;
       }
   }
    }



template<class T>
void ChunkList<T>::Remove(T value) {
    if(numChunks == 0){
        throw EmptyList();
    }
    Node* traverser = head;  //traverser is a traveling pointer
    Node* prev = nullptr;    //prev is a pointer that follows traverser

    while(traverser != nullptr){                                                       //Keep traversing the linked list if you cannot find a match
        int i=0;                                                                      //initialize i here because we will use the value later outside of the for loop
        for(i=0; i<traverser->len; i++) {                                                 //run through the array of values to try to find a match
            if ((traverser->values[i] == value) && ((traverser->len) > 1)) {         //if we found a match and its not the only value in the array
                for(int j=i; j<(traverser->len)-1; j++) {                           //start the loop at the match and go through until the length of that array
                    traverser->values[j] = traverser->values[j+1];          //shift each value up in the array, this will override the value we are trying to remove
                }
                traverser->len--;                                                //Update the length of the array we just messed with
                listLen--;                                                  //Update the total length of the value list
                return;
            }
            else if ((traverser->values[i] == value) && ((traverser->len) == 1)){   //if we found a match, and its the only value in the array
                if(prev != nullptr) {                                               //if the match is not in the very first node of the list
                    prev->next = traverser->next;                                   //link the previous node with the next node before deleting the middle node
                }  else {                                                           //if we found a match and its the very first node in the list
                    head = traverser->next;                                         //relink the head to the 2nd node before deleting the first one
                }
                if(tail == traverser) {                                             //if the found value is in the very last node of the list
                    tail = prev;                                                    //relink the tail to the node right before the last before deleting last node
                }
                delete traverser;                                                   //delete the node
                numChunks--;                                                        //decrement the number of nodes in the list
                listLen--;                                                          //decrement the total number of values in the list
                return;
            }
        }
        prev = traverser;                                                      //Move the trailer pointer up if we have not found the value yet
        traverser = traverser->next;                                          //if there was no match in that array, move the traverser to the next node
    }
}

template<class T>
int ChunkList<T>::GetLength()
{return listLen;}

template<class T>
double ChunkList<T>::LoadFactor() {
    if(numChunks != 0) {
        double loadPercent = static_cast<double>(listLen) / (ARRAY_SIZE*numChunks);   //Calculates the percentage of the lists that are filled with values
        return loadPercent;    //return that value
    } else {
        throw EmptyList();     //throw emptylist if there are no 0 numchunks or else program will attempt to divide by 0
    }
}

template<class T>
bool ChunkList<T>::Contains(T value) {
    if(numChunks == 0){                             //if there are no nodes in the list throw empty list error
        throw EmptyList();
    }
    Node* traverser = head;                        //Start the traveling pointer at the beginning of the list

    while(traverser != nullptr){                  //the traveling pointer will keep going until it finds the value or reaches end of the list
        int i=0;
        for(i=0; i<traverser->len; i++){         //go through each value of the array
            if(traverser->values[i] == value){   //if we find the value, we are done
                return true;
            }
        }
        traverser = traverser->next;            //if the value wasn't in this node then move on to the next one
    }
    return false;                              //return false if we have gone through each node and not found the value
}

template<class T>
T ChunkList<T>::GetIndex(int i) {
    if(numChunks == 0) {                                //if there are 0 nodes in the list throw an emptylist error message
        throw EmptyList();
    }
    if((i >= listLen) || i<0){                          //if the given index is negative, greater than or equal to the total number of items in the list
        throw IndexOutOfBounds();                       //throw an index out of bounds error message
    }

    Node* traverser = head;                             //set the traveling pointer at the beginning of the list
    while(traverser != nullptr) {                       //continue traveling until the end of the list is reached (worst case)
       if(i<traverser->len){                            //if the given index is less than the length of whatever node we are on
           return traverser->values[i];                 //return the value held at that array position
           }

       else {                                           //if the given index is greater than or equal to the length of the array we are at
           i = i - traverser->len;                      //subtract the length of the array from the given index
           traverser = traverser->next;                 //Move the traveling pointer to the next node and try again with the updated index value
           }
       }
    }

template<class T>
void ChunkList<T>::ResetIterator() {   //Resets iterNode to the head and resets the array position to the first element of the array
    iterNode = head;
    arrPos = 0;
}

template<class T>
T ChunkList<T>::GetNextItem() {                             //Returns the next item in the list
    if(iterNode == nullptr){                                //If the list is empty throw an iterator out of bounds error message
        throw IteratorOutOfBounds();
    }
    while(iterNode != nullptr) {                            //iterNode will travel through the list until it reaches the end (worst case)
        if (arrPos < iterNode->len) {                       //if the array position is less than the value of the length of the node we are on
            return iterNode->values[arrPos++];              //Return that value and move the array position up one index

        } else {                                            //if the array position has been incremented higher than the length of the array - 1
            if((iterNode->next == nullptr) && (arrPos > ((iterNode->len)-1))){   //Also, if we are on the very last node and there are no more indeces in the array
                throw IteratorOutOfBounds();                                     //We are out of bounds, so throw the Iterator out of bounds error message
            }
                iterNode = iterNode->next;                                       //Move the iterNode to the next node given there are still nodes in the list
                arrPos = 0;                                                      //Reset the array position back to 0
        }
    }
}

template<class T>
bool ChunkList<T>::IsEmpty() {        //Checks to see if a list is empty
    if(numChunks == 0){
        return true;
    } else {
        return false;
    }
}
